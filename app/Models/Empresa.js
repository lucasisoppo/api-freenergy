"use strict";

const Model = use("Model");

class Empresa extends Model {
  static boot() {
    super.boot();

    this.addTrait("@provider:Lucid/Slugify", {
      fields: { database: "nome" },
      strategy: "dbIncrement",
      disableUpdates: false,
    });
  }

  usuarios() {
    return this.belongsToMany("App/Models/Usuario").pivotModel(
      "App/Models/UsuarioEmpresa"
    );
  }

  grupos() {
    return this.hasMany("App/Models/Grupo");
  }

  setors() {
    return this.hasMany("App/Models/Setor");
  }

  clientes() {
    return this.hasMany("App/Models/Cliente");
  }

  equipamentos() {
    return this.hasMany("App/Models/Equipamento");
  }

  consumos() {
    return this.hasMany("App/Models/Consumo");
  }

  concessionaria() {
    return this.belongsTo("App/Models/Concessionaria");
  }
}

module.exports = Empresa;
