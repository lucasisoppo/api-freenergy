'use strict'

const Model = use('Model')

class Consumo extends Model {
  equipamento () {
    return this.belongsTo('App/Models/Equipamento')
  }
}

module.exports = Consumo
