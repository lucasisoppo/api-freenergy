'use strict'

const Model = use('Model')

class Equipamento extends Model {
  grupo () {
    return this.belongsTo('App/Models/Grupo')
  }
  setor () {
    return this.belongsTo('App/Models/Setor')
  }
}

module.exports = Equipamento
