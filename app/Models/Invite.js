'use strict'

const Model = use('Model')

class Invite extends Model {
  static boot () {
    super.boot()
    this.addHook('afterCreate', 'InviteHook.sendInvitationEmail')
  }

  usuario () {
    return this.belongsTo('App/Models/Usuario')
  }

  empresa () {
    return this.belongsTo('App/Models/Empresa')
  }
}

module.exports = Invite
