'use strict'

const Model = use('Model')

class UsuarioEmpresa extends Model {
  static get traits () {
    return [
      '@provider:Adonis/Acl/HasRole',
      '@provider:Adonis/Acl/HasPermission'
    ]
  }

  roles () {
    return this.belongsToMany('Adonis/Acl/Role')
  }

  permission () {
    return this.belongsToMany('Adonis/Acl/Permission')
  }

  usuario () {
    return this.belongsTo('App/Models/Usuario')
  }
}

module.exports = UsuarioEmpresa
