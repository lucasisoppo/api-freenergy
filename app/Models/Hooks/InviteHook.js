'use strict'

const Usuario = use('App/Models/Usuario')
const Kue = use('Kue')
const Job = use('App/Jobs/InvitationEmail')

const InviteHook = (exports = module.exports = {})

InviteHook.sendInvitationEmail = async invite => {
  const { email } = invite
  const invited = await Usuario.findBy('email', email)

  if (invited) {
    await invited.empresas().attach(invite.empresa_id)
  } else {
    const usuario = await invite.usuario().fetch()
    const empresa = await invite.empresa().fetch()

    Kue.dispatch(Job.key, { usuario, empresa, email }, { attempts: 3 })
  }
}
