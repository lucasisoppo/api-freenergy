'use strict'

const Model = use('Model')
const Hash = use('Hash')

class Usuario extends Model {
  static boot () {
    super.boot()

    this.addHook('beforeSave', async (usuarioInstance) => {
      if (usuarioInstance.dirty.password) {
        usuarioInstance.password = await Hash.make(usuarioInstance.password)
      }
    })
  }

  empresaJoins () {
    return this.hasMany('App/Models/UsuarioEmpresa')
  }

  tokens () {
    return this.hasMany('App/Models/Token')
  }

  empresas () {
    return this.belongsToMany('App/Models/Empresa').pivotModel(
      'App/Models/UsuarioEmpresa'
    )
  }

  async is (expression) {
    const empresa = await this.empresaJoins()
      .where('empresa_id', this.currentEmpresa)
      .first()

    return empresa.is(expression)
  }

  async can (expression) {
    const empresa = await this.empresaJoins()
      .where('empresa_id', this.currentEmpresa)
      .first()

    return empresa.can(expression)
  }

  async scope (required) {
    const empresa = await this.empresaJoins()
      .where('empresa_id', this.currentEmpresa)
      .first()

    return empresa.scope(required)
  }
}

module.exports = Usuario
