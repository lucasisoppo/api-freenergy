'use strict'

const Mail = use('Mail')

class InvitationEmail {
  static get concurrency () {
    return 1
  }

  static get key () {
    return 'InvitationEmail-job'
  }

  async handle ({ usuario, empresa, email }) {
    await Mail.send(
      ['emails.convite_sistema'],
      { empresa: empresa.nome, usuario: usuario.nome },
      message => {
        message
          .to(email)
          .from('suporte.nummus@gmail.com', 'Suporte Nummus')
          .subject(`Convite da empresa ${empresa.nome}`)
      }
    )
  }
}

module.exports = InvitationEmail
