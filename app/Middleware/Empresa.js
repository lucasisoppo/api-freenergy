'use strict'

class Empresa {
  async handle ({ request, response, auth }, next) {
    const database = request.header('EMPRESA')

    let empresa = null

    if (database) {
      empresa = await auth.user.empresas().where('database', database).first()
    }

    if (!empresa) {
      return response
        .status(401)
        .send({ error: { message: 'O e-mail do usuário não tem permissão.' } })
    }

    auth.user.currentEmpresa = empresa.id
    request.empresa = empresa

    await next()
  }
}

module.exports = Empresa
