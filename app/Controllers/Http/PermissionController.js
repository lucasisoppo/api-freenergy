'use strict'

const UsuarioEmpresa = use('App/Models/UsuarioEmpresa')

class PermissionController {
  async show ({ request, auth }) {
    const empresaJoin = await UsuarioEmpresa.query()
      .where('empresa_id', request.empresa.id)
      .where('usuario_id', auth.user.id)
      .first()

    return {
      roles: await empresaJoin.getRoles(),
      permissions: await empresaJoin.getPermissions()
    }
  }
}

module.exports = PermissionController
