'use strict'

const Database = use('Database')

class DashboardController {
  async index ({ response, request, auth }) {
    const { id: empresaId } = request.empresa
    // const usuario = await auth.authenticator('usuario').getUser()
    // const data = await Database.raw(`
    //   select id from usuario_empresas ue where usuario_id = ${usuario.id} and empresa_id = ${empresaId} and coalesce("statusUser", 'A') <> 'D'
    // `)

    // if (data.rowCount == 0) {
    //   return response
    //     .status(401)
    //     .send({ error: { message: 'Sem permissão de acesso' } })
    // }

    const consumoPorGrupos = await this.consumoPorGrupos(empresaId)
    const consumoGruposPorSetor = await this.consumoGruposPorSetor(empresaId)
    const consumoMensal = await this.consumoMensal(empresaId)

    return {
      consumoPorGrupos,
      consumoGruposPorSetor,
      consumoMensal
    }
  }

  async consumoPorGrupos (empresaId) {
    let sql = `
      select
        g.descricao, sum(c.custo_total) "valorGasto", sum(c.kwh) "kwh"
      from consumos c
      left join equipamentos e on c.equipamento_id = e.id
      left join grupos g on e.grupo_id = g.id
      where c.empresa_id = ${empresaId}
      and substring(c.data_hora, 1, 4) = 
      (SELECT substring(MAX(c2.data_hora), 1, 4)
       FROM consumos c2)
       
      group by g.descricao 
      order by 3 desc
    `
    const data = await Database.raw(sql)

    return data.rows
  }

  async consumoGruposPorSetor (empresaId) {
    let sqlSetores = `select s.descricao "setor" from setors s where s.empresa_id = ${empresaId}`
    const dataSetores = await Database.raw(sqlSetores)
    var setores = dataSetores.rows

    let sqlGrupos = `select g.descricao "grupo" from grupos g where g.empresa_id = ${empresaId}`
    const dataGrupos = await Database.raw(sqlGrupos)
    var grupos = dataGrupos.rows

    let sql = `
      select
      s.descricao "setor", g.descricao "grupo", sum(c.custo_total) "valorGasto", sum(c.kwh) "kwh"
      from consumos c
      left join equipamentos e on c.equipamento_id = e.id
      left join setors s on e.setor_id = s.id
      left join grupos g on e.grupo_id = g.id
      where c.empresa_id = ${empresaId}
      and substring(c.data_hora, 1, 4) = 
      (SELECT substring(MAX(c2.data_hora), 1, 4)
       FROM consumos c2)
      
      group by g.descricao, setor 
      order by 3 desc
    `
    const data = await Database.raw(sql)
    var consulta = data.rows
    setores.forEach(setor => {
      setor['grupos'] = grupos
      grupos.forEach(grupo => {
        setor[grupo.grupo] = 0
      })
      consulta.forEach(consulta => {
        if (setor.setor === consulta.setor) { 
          setor[consulta.grupo] = consulta.valorGasto
        }
      })
    })
    return setores
  }

  async consumoMensal (empresaId) {
    let sql = `
        select t.indice as indice, max(t.mes) as mes, max(t.anoAtual) as anoAtual, max(t.anoAnt) as anoAnt, sum(t.totalAnoAtual) as totalAnoAtual, sum(t.totalAnoAnt) as totalAnoAnt from 
          (
            ( select t1.indice, t1.mes, 
                (select (MAX(CAST(substring(data_hora, 1, 4) AS INT))) from consumos where empresa_id = ${empresaId}) as anoAtual, 
                (select (MAX(CAST(substring(data_hora, 1, 4) AS INT)) -1 ) from consumos where empresa_id = ${empresaId}) as anoAnt, 0 as totalAnoAtual, 0 as totalAnoAnt from 
              (
              select '01' as indice, 'JAN' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt 
              union all 
              select '02' as indice, 'FEV' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt 
              union all 
              select '03' as indice, 'MAR' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt 
              union all 
              select '04' as indice, 'ABR' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt 
              union all 
              select '05' as indice, 'MAI' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt 
              union all 
              select '06' as indice, 'JUN' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt 
              union all 
              select '07' as indice, 'JUL' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt  
              union all 
              select '08' as indice, 'AGO' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt 
              union all 
              select '09' as indice, 'SET' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt 
              union all 
              select '10' as indice, 'OUT' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt  
              union all 
              select '11' as indice, 'NOV' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt 
              union all 
              select '12' as indice, 'DEC' as mes, 0 as anoAtual, 0 as anoAnt, 0 as totalAnoAtual, 0  as totalAnoAnt 
              )as t1
              group by 1,2
              order by 1 asc
            )
            
            union all
            
            (select substring(c.data_hora, 6, 2) as indice , null, (CAST(substring(c.data_hora, 1, 4) AS INT)) as anoAtual, (MAX(CAST(substring(c.data_hora, 1, 4) AS INT)) -1) as anoAnt, sum(c.custo_total) as totalAnoAtual, 0 as totalAnoAnt 
            from consumos c 
            --busca do ano da ultima leitura feita
            where c.empresa_id = ${empresaId}
            and substring(c.data_hora, 1, 4) = 
              (SELECT substring(MAX(c2.data_hora), 1, 4)
                FROM consumos c2)
            group by 1, 3
            order by 1 asc )
            
            union all
            
            (select substring(c.data_hora, 6, 2) , null, (MAX(CAST(substring(c.data_hora, 1, 4) AS INT)) +1) as anoAtual, (CAST(substring(c.data_hora, 1, 4) AS INT)) as anoAnt, 0 as totalAnoAtual, sum(c.custo_total) as totalAnoAnt 
            from consumos c 
            --busca do ano da ultima leitura feita
            where c.empresa_id = ${empresaId}
            and CAST(substring(c.data_hora, 1, 4) AS INT) = 
              (SELECT MAX(CAST(substring(c3.data_hora, 1, 4) AS INT)) -1
                FROM consumos c3)
            group by 1, 4
            order by 1 asc)
          ) as t
        group by 1  
        order by 1 asc 
      `
    const data = await Database.raw(sql)

    return data.rows
  }
}

module.exports = DashboardController
