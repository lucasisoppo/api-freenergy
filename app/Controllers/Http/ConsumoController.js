'use strict'

const Concessionaria = use('App/Models/Concessionaria')
const Equipamento = use('App/Models/Equipamento')
const Grupo = use('App/Models/Grupo')
const moment = require('moment')

class ConsumoController {
  async index ({ request }) {
    return new Promise(async resolve => {
      const { page, perPage } = request.params
      const { filter } = request.get()

      const consumos = request.empresa
        .consumos()
        .with('equipamento')
        .orderBy('id', 'desc')

      if (filter) {
        consumos.orWhereHas('equipamento', builder => {
          builder.whereRaw(`upper(descricao) like upper('%${filter}%')`)
        })
      }

      resolve(await consumos.paginate(page, perPage))
    })
  }

  // POST consumos
  async store ({ request }) {
    const data = request.only(['equipamento_id', 'media_corrente', 'duracao', 'data_hora'])

    const equipamento = await Equipamento.find(data.equipamento_id)
    const grupo = await Grupo.find(equipamento.grupo_id)
    data.media_potencia = this.getMediaPotencia(data.media_corrente, grupo)
    data.kwh = this.getKwH(data.duracao, data.media_potencia)

    const concessionaria = await Concessionaria.find(request.empresa.concessionaria_id)
    const tarifa = await this.getTarifa(concessionaria, request.empresa.tarifa_branca, data.data_hora)
   
    data.custo_total = this.getCusto(data.kwh, tarifa)

    const consumo = await request.empresa.consumos().create(data)

    await consumo.load('equipamento')

    return consumo
  }

  // GET consumos/:id
  async show ({ params, request }) {
    const consumo = await request.empresa
      .consumos()
      .with('equipamento')
      .where('id', params.id)
      .first()

    return consumo
  }

  // DELETE consumos/:id
  async destroy ({ params, request }) {
    const consumo = await request.empresa
      .consumos()
      .where('id', params.id)
      .first()

    await consumo.delete()
  }

  getMediaPotencia (mediaCorrente, grupo) {
    const tensao = grupo.tensao
    // potencia (watts) = corrente (hamperes) * tensao (volts)
    var mediaPotencia = mediaCorrente * tensao
    return mediaPotencia
  }

  getTarifa (concessionaria, istarifabranca, dataHora) {
    var tarifa = concessionaria.vlrtotatrfconvencional
    if (istarifabranca) {
      var format = 'HH:mm:ss'
      var dateTime = moment(dataHora).format(format)
      const time = moment(dateTime, format)
      const iniPonta = moment(concessionaria.hiniponta, format)
      const fimPonta = moment(concessionaria.hfimponta, format)
      const iniForaPonta = moment(concessionaria.hiniforaponta, format)
      const fimForaPonta = moment(concessionaria.hfimforaponta, format)
      if (time.isAfter(iniForaPonta) || time.isBefore(fimForaPonta)) {
        tarifa = concessionaria.vlrtrfbrancaforaponta
      } else if (time.isBetween(iniPonta, fimPonta)) {
        tarifa = concessionaria.vlrtrfbrancaponta
      } else {
        tarifa = concessionaria.vlrtrfbrancaintermediaria
      }
    }
    return tarifa
  }

  getCusto (kwH, tarifa) {
    // custo = KWh * tarifa
    var custo = tarifa * kwH
    return custo
  }

  getKwH (tempo, mediaPotencia) {
    // Wh = tempo (horas) * potencia (watts)
    // KWh = Wh / 1000
    const tempoHoras = tempo / 3600
    var kwh = (mediaPotencia * tempoHoras) / 1000
    return kwh
  }
}

module.exports = ConsumoController
