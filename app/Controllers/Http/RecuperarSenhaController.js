'use strict'

const moment = require('moment')
const crypto = require('crypto')
const Usuario = use('App/Models/Usuario')
const Mail = use('Mail')

class RecuperarSenhaController {
  async store ({ request, response }) {
    try {
      const email = request.input('email')
      const usuario = await Usuario.findByOrFail('email', email)

      usuario.token = crypto.randomBytes(10).toString('hex')
      usuario.token_created_at = new Date()

      await usuario.save()

      await Mail.send(
        ['emails.recuperar_senha'],
        { email, token: usuario.token, link: `${request.input('redirect_url')}?token=${usuario.token}` },
        message => {
          message
            .to(usuario.email)
            .from('suporte.nummus@gmail.com', 'Suporte Nummus')
            .subject('Recuperação de senha | NUMMUS SISTEMAS')
        }
      )
    } catch (err) {
      return response
        .status(err.status)
        .send({ error: { mensage: 'O e-mail digitado não existe.' } })
    }
  }
  async update ({ request, response }) {
    try {
      const { token, password, confirmPassword } = request.all()

      if (password !== confirmPassword) {
        return response
          .status(400)
          .send({ error: { message: 'A nova senha não confere com a senha de confirmação.' } })
      }

      const usuario = await Usuario.findByOrFail('token', token)

      const tokenExpired = moment()
        .subtract('1', 'days')
        .isAfter(usuario.token_created_at)

      if (tokenExpired) {
        return response
          .status(401)
          .send({
            error: { message: 'O token de recuperação está expirado' }
          })
      }
      usuario.token = null
      usuario.token_created_at = null
      usuario.password = password

      await usuario.save()
    } catch (err) {
      return response
        .status(err.status)
        .send({ error: { message: 'Erro. O token digitado é diferente do token enviado para resetar a senha' } })
    }
  }
}

module.exports = RecuperarSenhaController
