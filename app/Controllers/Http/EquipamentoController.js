'use strict'

class EquipamentoController {
  async index ({ request }) {
    return new Promise(async resolve => {
      const { page, perPage } = request.params
      const { filter } = request.get()

      const equipamentos = request.empresa
        .equipamentos()
        .with('grupo')
        .with('setor')
        .orderBy('id', 'desc')

      if (filter) {
        equipamentos.orWhereHas('grupo', builder => {
          builder.whereRaw(`upper(descricao) like upper('%${filter}%')`)
        })
      }

      resolve(await equipamentos.paginate(page, perPage))
    })
  }

  // POST equipamentos
  async store ({ request }) {
    const data = request.only(['setor_id', 'grupo_id', 'descricao', 'potencia'])
    const equipamento = await request.empresa.equipamentos().create(data)

    await equipamento.load('grupo')
    await equipamento.load('setor')

    return equipamento
  }

  // GET equipamentos/:id
  async show ({ params, request }) {
    const equipamento = await request.empresa
      .equipamentos()
      .with('grupo')
      .with('setor')
      .where('id', params.id)
      .first()

    return equipamento
  }

  // PUT or PATCH equipamentos/:id
  async update ({ params, request }) {
    const data = request.only(['setor_id', 'grupo_id', 'descricao', 'potencia'])
    const equipamento = await request.empresa
      .equipamentos()
      .where('id', params.id)
      .first()

    equipamento.merge(data)

    await equipamento.save()
    await equipamento.load('grupo')
    await equipamento.load('setor')

    return equipamento
  }

  // DELETE equipamentos/:id
  async destroy ({ params, request }) {
    const equipamento = await request.empresa
      .equipamentos()
      .where('id', params.id)
      .first()

    await equipamento.delete()
  }
}

module.exports = EquipamentoController
