"use strict";

const Database = use("Database");

class SetorController {
  // GET setors
  async index({ request }) {
    const { page, perPage } = request.params;
    let { filter } = request.get();

    const setors = await request.empresa
      .setors()
      .where(Database.raw(`upper(descricao) like upper('%${filter || ""}%')`))
      .orderBy("id", "desc")
      .paginate(page, perPage);

    return setors;
  }

  // POST setors
  async store({ request }) {
    const data = request.only(["descricao"]);
    const setor = request.empresa.setors().create(data);

    return setor;
  }

  // GET setors/:id
  async show({ params, request }) {
    const setor = await request.empresa.setors().where("id", params.id).first();

    return setor;
  }

  // PUT or PATCH setors/:id
  async update({ params, request }) {
    const data = request.only(["descricao"]);
    const setor = await request.empresa.setors().where("id", params.id).first();

    setor.merge(data);

    await setor.save();

    return setor;
  }

  // DELETE setors/:id
  async destroy({ params, request }) {
    const setor = await request.empresa.setors().where("id", params.id).first();

    await setor.delete();
  }
}

module.exports = SetorController;
