'use strict'

const Usuario = use('App/Models/Usuario')
const Invite = use('App/Models/Invite')
const Hash = use('Hash')

class UsuarioController {
  async show ({ auth }) {
    const usuario = await auth.getUser()

    return usuario
  }

  async store ({ request, response, auth }) {
    const data = request.only(['nome', 'cpf', 'email', 'password'])
    const confirmPassword = request.input('confirmPassword')

    const empresasQuery = Invite.query().where('email', data.email)
    const empresas = await empresasQuery.pluck('empresa_id')

    if (empresas.length === 0) {
      return response
        .status(401)
        .send({ message: 'Você não tem permissão em nenhuma empresa.' })
    }

    if (data.password !== confirmPassword) {
      return response
        .status(400)
        .send({ error: { message: 'A senha não confere com a senha de confirmação.' } })
    }

    const usuario = await Usuario.create(data)

    await usuario.empresas().attach(empresas)

    const empresaJoin = await usuario
      .empresaJoins()
      .where('empresa_id', 1).first()
    await empresaJoin.roles().sync([2])

    await empresasQuery.delete()

    const token = await auth.attempt(data.email, data.password)

    return token
  }

  async update ({ request, response, auth }) {
    const data = request.only(['nome', 'cpf', 'email'])
    const currentUser = await auth.getUser()

    if (currentUser.email !== data.email) {
      const emailExists = await Usuario.findBy('email', data.email)

      if (emailExists) {
        return response
          .status(400)
          .send({ error: { message: 'E-mail já existente.' } })
      }
    }

    const usuario = await Usuario.findBy('id', currentUser.id)
    usuario.merge(data)
    await usuario.save()

    return usuario
  }

  async alterPassword ({ request, response, auth }) {
    const data = request.only(['oldPassword', 'password', 'confirmPassword'])
    const currentUser = await auth.getUser()

    if (!await Hash.verify(data.oldPassword, currentUser.password)) {
      return response
        .status(400)
        .send({ error: { message: 'A senha antiga não confere.' } })
    }

    if (data.password !== data.confirmPassword) {
      return response
        .status(400)
        .send({ error: { message: 'A nova senha não confere com a senha de confirmação.' } })
    }

    const usuario = await Usuario.findBy('id', currentUser.id)

    const dataUpdate = {
      password: data.password
    }

    usuario.merge(dataUpdate)
    await usuario.save()

    return response
      .status(200)
      .send({ message: 'Senha alterada com sucesso!' })
  }
}

module.exports = UsuarioController
