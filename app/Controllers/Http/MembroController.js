'use strict'

const UsuarioEmpresa = use('App/Models/UsuarioEmpresa')

class MembroController {
  async index ({ request }) {
    const membros = await UsuarioEmpresa.query()
      .where('empresa_id', request.empresa.id)
      .with('usuario')
      .with('roles')
      .fetch()

    return membros
  }

  async update ({ request, params }) {
    const roles = request.input('roles')

    const empresaJoin = await UsuarioEmpresa.find(params.id)

    await empresaJoin.roles().sync(roles)
  }
}

module.exports = MembroController
