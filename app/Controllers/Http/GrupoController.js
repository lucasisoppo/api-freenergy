'use strict'

const Database = use('Database')

class GrupoController {
  // GET grupos
  async index ({ request }) {
    const { page, perPage } = request.params
    let { filter } = request.get()

    const grupos = await request.empresa
      .grupos()
      .where(
        Database.raw(
          `upper(descricao) like upper('%${filter ||
            ''}%')`
        )
      )
      .orderBy('id', 'desc')
      .paginate(page, perPage)

    return grupos
  }

  // POST grupos
  async store ({ request }) {
    const data = request.only(['descricao', 'tensao'])
    const grupo = request.empresa.grupos().create(data)

    return grupo
  }

  // GET grupos/:id
  async show ({ params, request }) {
    const grupo = await request.empresa
      .grupos()
      .where('id', params.id)
      .first()

    return grupo
  }

  // PUT or PATCH grupos/:id
  async update ({ params, request }) {
    const data = request.only(['descricao', 'tensao'])
    const grupo = await request.empresa
      .grupos()
      .where('id', params.id)
      .first()

    grupo.merge(data)

    await grupo.save()

    return grupo
  }

  // DELETE grupos/:id
  async destroy ({ params, request }) {
    const grupo = await request.empresa
      .grupos()
      .where('id', params.id)
      .first()

    await grupo.delete()
  }
}

module.exports = GrupoController
