'use strict'

const Concessionaria = use('App/Models/Concessionaria')

class ConcessionariaController {
  async index ({ request }) {
    const concessionarias = await Concessionaria.query()
      .fetch()

    return concessionarias
  }
}

module.exports = ConcessionariaController
