'use strict'
const Invite = use('App/Models/Invite')

class InviteController {
  // POST invites
  async store ({ request, auth }) {
    const invites = request.input('invites')

    const data = invites.map(email => ({
      email,
      usuario_id: auth.user.id,
      empresa_id: request.empresa.id
    }))
    await Invite.createMany(data)
  }
}
module.exports = InviteController
