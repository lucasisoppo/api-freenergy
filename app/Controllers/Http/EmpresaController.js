'use strict'

const Role = use('Adonis/Acl/Role')
const Database = use('Database')

class EmpresaController {
  // GET empresas
  async index ({ auth, request }) {
    const { page, perPage } = request.params
    let { filter } = request.get()
    const empresas = await auth.user
      .empresas()
      .with('concessionaria')
      .where(Database.raw(`upper(nome||cnpj||endereco||cidade||uf||telefone||email) like upper('%${filter || ''}%')`))
      .orderBy('id', 'desc')
      .paginate(page, perPage)

    return empresas
  }

  // POST empresas
  async store ({ request, auth }) {
    const data = request.only([
      'nome',
      'cnpj',
      'endereco',
      'cidade',
      'uf',
      'cep',
      'telefone',
      'celular',
      'email',
      'concessionaria_id',
      'tarifa_branca'
    ])

    const empresa = await auth.user.empresas().create({
      ...data,
      usuario_id: auth.user.id
    })

    await empresa.load('concessionaria')

    const empresaJoin = await auth.user
      .empresaJoins()
      .where('empresa_id', empresa.id)
      .first()

    const admin = await Role.findBy('slug', 'admin')

    await empresaJoin.roles().attach([admin.id])

    return empresa
  }

  // GET empresas/:id
  async show ({ params, auth }) {
    const empresa = await auth.user
      .empresas()
      .with('concessionaria')
      .where('empresas.id', params.id)
      .first()

    return empresa
  }

  // PUT or PATCH empresas/:id
  async update ({ params, request, auth }) {
    const data = request.only([
      'nome',
      'cnpj',
      'endereco',
      'cidade',
      'uf',
      'cep',
      'telefone',
      'celular',
      'email',
      'concessionaria_id',
      'tarifa_branca'
    ])
    const empresa = await auth.user
      .empresas()
      .where('empresas.id', params.id)
      .first()

    await empresa.load('concessionaria')

    empresa.merge(data)

    await empresa.save()

    return empresa
  }

  // DELETE empresas/:id
  async destroy ({ params, auth }) {
    const empresa = await auth.user
      .empresas()
      .where('empresas.id', params.id)
      .first()

    await empresa.delete()
  }
}

module.exports = EmpresaController
