'use strict'

class UsuarioUpdate {
  get validateAll () {
    return true
  }
  get rules () {
    return {
      nome: 'required',
      cpf: 'required'
    }
  }
}

module.exports = UsuarioUpdate
