'use strict'

class UsuarioAlterPassword {
  get validateAll () {
    return true
  }
  get rules () {
    return {
      oldPassword: 'required',
      password: 'required',
      confirmPassword: 'required'
    }
  }
}

module.exports = UsuarioAlterPassword
