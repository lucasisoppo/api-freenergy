'use strict'

class Empresa {
  get validateAll () {
    return true
  }
  get rules () {
    return {
      nome: 'required',
      cnpj: 'required',
      endereco: 'required',
      cidade: 'required',
      uf: 'required',
      email: 'required|email'
    }
  }
}

module.exports = Empresa
