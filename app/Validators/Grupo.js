'use strict'

class Config {
  get validateAll () {
    return true
  }
  get rules () {
    return {
      descricao: 'required',
      tensao: 'required'
    }
  }
}

module.exports = Config
