'use strict'

class Equipamento {
  get validateAll () {
    return true
  }
  get rules () {
    return {
      setor_id: 'required',
      grupo_id: 'required',
      potencia: 'required'
    }
  }
}

module.exports = Equipamento
