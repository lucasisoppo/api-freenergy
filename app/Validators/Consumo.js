'use strict'

class Consumo {
  get validateAll () {
    return true
  }
  get rules () {
    return {
      equipamento_id: 'required',
      media_corrente: 'required',
      duracao: 'required',
      data_hora: 'required'
    }
  }
}

module.exports = Consumo
