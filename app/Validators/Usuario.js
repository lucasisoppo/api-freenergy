'use strict'

class Usuario {
  get validateAll () {
    return true
  }
  get rules () {
    return {
      nome: 'required',
      email: 'required|email|unique:usuarios',
      password: 'required',
      cpf: 'required|unique:usuarios'
    }
  }
}

module.exports = Usuario
