#  api-freenergy

1. Autenticação
2. CORS
3. Migrations and seeds
4. Mult-Tenant
5. Permissão de usuário
6. Convite de usuário (Quando o usuário não for cadastrado no sistema, um e-mail é enviado para o usuário)

## Install
`$ npm install`

### Migrations

`$ adonis migration:run`

`$ adonis migration:rollback`

### Seed

`$ adonis seed`

### START

`$ adonis serve --dev`

### FILA DE E-MAILS

`$ adonis kue:listen`

