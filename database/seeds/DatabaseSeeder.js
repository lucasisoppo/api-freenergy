'use strict'

const Usuario = use('App/Models/Usuario')
const Concessionaria = use('App/Models/Concessionaria')
const Consumo = use('App/Models/Consumo')
const Grupo = use('App/Models/Grupo')
const Setor = use('App/Models/Setor')
const Equipamento = use('App/Models/Equipamento')
const Role = use('Adonis/Acl/Role')
const Permission = use('Adonis/Acl/Permission')

class DatabaseSeeder {
  async run () {
    const usuario = await Usuario.create({
      nome: 'João Manoel Aureliano',
      email: 'joao@nummus.com.br',
      cpf: '090.571.159.99',
      password: '12345678'
    })

    const createEquipamento = await Permission.create({
      slug: 'equipamento_create',
      nome: 'Cadastro de equipamentos',
      descricao: 'Permissão para cadastrar equipamentos '
    })

    const admin = await Role.create({
      slug: 'admin',
      nome: 'Administrador',
      descricao: 'Permissão de administrador'
    })

    const colaborador = await Role.create({
      slug: 'colaborador',
      nome: 'Colaborador',
      descricao: 'Permissão de colaborador'
    })

    const concessionaria = await Concessionaria.create({
      id: '533',
      concessao: 'Concessionária',
      distribuidora: 'Celesc-DIS',
      regiao: 'S ',
      vlrtusdconvencional: '0.22236',
      vlrteconvencional: '0.24742',
      vlrtotatrfconvencional: '0.46978',
      vlrtrfbrancaponta: '0.83916',
      vlrtrfbrancaintermediaria: '0.53394',
      vlrtrfbrancaforaponta: '0.39765',
      hiniponta: '18:30:00',
      hfimponta: '21:30:00',
      hiniforaponta: '22:30:00',
      hfimforaponta: '17:30:00' 
    })

    await admin.permissions().attach([
      createEquipamento.id
    ])

    await colaborador
      .permissions()
      .attach([createEquipamento.id])

    const empresa = await usuario.empresas().create({
      nome: 'Mercado Brasil',
      cnpj: '00.456.865/0001-60',
      endereco: 'R. Major acácio moreira, 388 - centro',
      cidade: 'Criciúma',
      uf: 'SC',
      cep: '888.01-160',
      telefone: '(48) 9 9986-2540',
      celular: '(48) 9 9986-2540',
      email: 'mercado@brasil.com.br',
      usuario_id: usuario.id,
      concessionaria_id: concessionaria.id,
      tarifa_branca: false
    })

    const empresaJoin = await usuario
      .empresaJoins()
      .where('empresa_id', empresa.id)
      .first()

    await empresaJoin.roles().attach([admin.id])

    const setorDesenvolvimento = await Setor.create({
      descricao: 'Desenvolvimento',
      empresa_id: empresa.id
    })

    const setorInfra = await Setor.create({
      descricao: 'Infra',
      empresa_id: empresa.id
    })

    const grupoDesenvolvimento = await Grupo.create({
      descricao: 'Computadores',
      tensao: '220',
      empresa_id: empresa.id
    })

    const grupoServidores = await Grupo.create({
      descricao: 'Servidores',
      tensao: '380',
      empresa_id: empresa.id
    })

    const equipamento1 = await Equipamento.create({
      descricao: 'Impressora Lucas',
      potencia: 400,
      grupo_id: grupoDesenvolvimento.id,
      setor_id: setorDesenvolvimento.id,
      empresa_id: empresa.id
    })

    const equipamento2 = await Equipamento.create({
      descricao: 'Servidor Dados',
      potencia: 600,
      grupo_id: grupoServidores.id,
      setor_id: setorInfra.id,
      empresa_id: empresa.id
    })

    // Consumo Impressora
    await Consumo.create({
      data_hora: '2019-09-27T02:08:15.756',
      media_corrente: 6.5,
      media_potencia: 1430,
      custo_total: 0.8293999999999999,
      kwh: 1.430,
      duracao: '3600.00',
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:08:15.756',
      media_corrente: 6.5,
      media_potencia: 1430,
      custo_total: 0.8293999999999999,
      kwh: 1.430,
      duracao: '3600.00',
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:09:15.756',
      media_corrente: 6.5,
      media_potencia: 1430,
      custo_total: 0.8293999999999999,
      kwh: 1.430,
      duracao: '3600.00',
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:10:15.756',
      media_corrente: 6.5,
      media_potencia: 1430,
      custo_total: 0.8293999999999999,
      kwh: 1.430,
      duracao: '3600.00',
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:08:15.756',
      media_corrente: 6.5,
      media_potencia: 1430,
      custo_total: 0.8293999999999999,
      kwh: 1.430,
      duracao: '3600.00',
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-09-27T02:08:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 1.8734,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-09-27T02:09:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 1.8734,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-09-27T02:10:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.9918,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-09-27T02:11:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.9918,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })


    await Consumo.create({
      data_hora: '2019-10-27T02:08:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 1.8734,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:09:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 1.8734,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:10:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.9918,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:11:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.9918,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:12:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.9918,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:13:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 1.8734,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:14:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 1.8734,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:08:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 1.8734,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:09:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 1.8734,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:10:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.9918,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:11:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.9918,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:12:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.9918,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:13:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 1.8734,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:14:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 1.8734,
      empresa_id: empresa.id,
      equipamento_id: equipamento1.id
    })

    // equipamento 2

    await Consumo.create({
      data_hora: '2019-09-27T02:13:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 0.969,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-09-27T02:14:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 0.969,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-09-28T02:08:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 0.969,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-09-28T02:09:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 0.969,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-09-28T02:10:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.513,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-09-28T02:11:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.513,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-09-28T02:12:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.513,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:13:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 0.969,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-10-27T02:14:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 0.969,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:08:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 0.969,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:09:15.756',
      media_corrente: 8.5,
      duracao: '3600.00',
      media_potencia: 3230,
      kwh: 3.230,
      custo_total: 0.969,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:10:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.513,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:11:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.513,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

    await Consumo.create({
      data_hora: '2019-10-28T02:12:15.756',
      media_corrente: 4.5,
      duracao: '3600.00',
      media_potencia: 1710,
      kwh: 1.710,
      custo_total: 0.513,
      empresa_id: empresa.id,
      equipamento_id: equipamento2.id
    })

  }
}

module.exports = DatabaseSeeder
