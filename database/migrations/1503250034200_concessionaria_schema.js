'use strict'

const Schema = use('Schema')

class ConcessionariaSchema extends Schema {
  up () {
    this.create('concessionarias', table => {
      table.increments()
      table.string('concessao').notNullable()
      table.string('distribuidora').notNullable()
      table.string('regiao').notNullable()
      table.decimal('vlrtusdconvencional', 10, 5).notNullable()
      table.decimal('vlrteconvencional', 10, 5).notNullable()
      table.decimal('vlrtotatrfconvencional', 10, 5).notNullable()
      table.decimal('vlrtrfbrancaponta', 10, 5).notNullable()
      table.decimal('vlrtrfbrancaintermediaria', 10, 5).notNullable()
      table.decimal('vlrtrfbrancaforaponta', 10, 5).notNullable()
      table.string('hiniponta').notNullable()
      table.string('hfimponta').notNullable()
      table.string('hiniforaponta').notNullable()
      table.string('hfimforaponta').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('concessionarias')
  }
}

module.exports = ConcessionariaSchema
