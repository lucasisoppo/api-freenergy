'use strict'

const Schema = use('Schema')

class ConsumoSchema extends Schema {
  up () {
    this.create('consumos', table => {
      table.increments()
      table.decimal('media_corrente').notNullable()
      table.decimal('media_potencia').notNullable()
      table.string('data_hora').notNullable()
      table.decimal('duracao').notNullable()
      table.decimal('custo_total').notNullable()
      table.decimal('kwh').notNullable()
      // .onDelete('CASCADE')
      table
        .integer('equipamento_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('equipamentos')
        .onUpdate('CASCADE')
      // .onDelete('CASCADE')
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
      // .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('consumos')
  }
}

module.exports = ConsumoSchema
