'use strict'

const Schema = use('Schema')

class EquipamentoSchema extends Schema {
  up () {
    this.create('equipamentos', table => {
      table.increments()
      table.string('descricao').notNullable()
      table.decimal('potencia').notNullable()
      // .onDelete('CASCADE')
      table
        .integer('grupo_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('grupos')
        .onUpdate('CASCADE')
      // .onDelete('CASCADE')
      table
        .integer('setor_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('setors')
        .onUpdate('CASCADE')
      // .onDelete('CASCADE')
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
      // .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('equipamentos')
  }
}

module.exports = EquipamentoSchema
