'use strict'

const Schema = use('Schema')

class InviteSchema extends Schema {
  up () {
    this.create('invites', table => {
      table.increments()
      table
        .integer('usuario_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('usuarios')
        .onUpdate('CASCADE')
        // .onDelete('CASCADE')
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
        // .onDelete('CASCADE')
      table.string('email').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('invites')
  }
}

module.exports = InviteSchema
