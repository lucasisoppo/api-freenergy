'use strict'

const Schema = use('Schema')

class RolesTableSchema extends Schema {
  up () {
    this.create('roles', table => {
      table.increments()
      table
        .string('slug')
        .notNullable()
        .unique()
      table
        .string('nome')
        .notNullable()
        .unique()
      table.string('descricao').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('roles')
  }
}

module.exports = RolesTableSchema
