'use strict'

const Schema = use('Schema')

class RoleUsuarioEmpresaTableSchema extends Schema {
  up () {
    this.create('role_usuario_empresa', table => {
      table.increments()
      table
        .integer('role_id')
        .unsigned()
        .index()
      table
        .foreign('role_id')
        .references('id')
        .on('roles')
        // .onDelete('cascade')
      table
        .integer('usuario_empresa_id')
        .unsigned()
        .index()
      table
        .foreign('usuario_empresa_id')
        .references('id')
        .on('usuario_empresas')
        // .onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('role_usuario_empresa')
  }
}

module.exports = RoleUsuarioEmpresaTableSchema
