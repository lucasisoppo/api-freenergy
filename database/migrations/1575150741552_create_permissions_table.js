'use strict'

const Schema = use('Schema')

class PermissionsTableSchema extends Schema {
  up () {
    this.create('permissions', table => {
      table.increments()
      table
        .string('slug')
        .notNullable()
        .unique()
      table
        .string('nome')
        .notNullable()
        .unique()
      table.string('descricao').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('permissions')
  }
}

module.exports = PermissionsTableSchema
