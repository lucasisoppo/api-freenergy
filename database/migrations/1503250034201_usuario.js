'use strict'

const Schema = use('Schema')

class UsuarioSchema extends Schema {
  up () {
    this.create('usuarios', table => {
      table.increments()
      table.string('nome').notNullable()
      table
        .string('cpf')
        .notNullable()
        .unique()
      table
        .string('email')
        .notNullable()
        .unique()
      table.string('password').notNullable()
      table.string('token')
      table.timestamp('token_created_at')
      table.timestamps()
    })
  }

  down () {
    this.drop('usuarios')
  }
}

module.exports = UsuarioSchema
