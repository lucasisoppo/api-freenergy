'use strict'

const Schema = use('Schema')

class PermissionUsuarioEmpresaTableSchema extends Schema {
  up () {
    this.create('permission_usuario_empresa', table => {
      table.increments()
      table
        .integer('permission_id')
        .unsigned()
        .index()
      table
        .foreign('permission_id')
        .references('id')
        .on('permissions')
        // .onDelete('cascade')
      table
        .integer('usuario_empresa_id')
        .unsigned()
        .index()
      table
        .foreign('usuario_empresa_id')
        .references('id')
        .on('usuario_empresas')
        // .onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('permission_usuario_empresa')
  }
}

module.exports = PermissionUsuarioEmpresaTableSchema
