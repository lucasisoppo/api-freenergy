'use strict'

const Schema = use('Schema')

class EmpresaSchema extends Schema {
  up () {
    this.create('empresas', table => {
      table.increments()
      table.string('nome').notNullable()
      table.string('cnpj').notNullable()
      table.string('endereco').notNullable()
      table.string('cidade').notNullable()
      table.string('uf').notNullable()
      table.string('cep')
      table.string('telefone')
      table.string('celular')
      table.string('email').notNullable()
      table.boolean('tarifa_branca')
      table
        .integer('usuario_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('usuarios')
        .onUpdate('CASCADE')
        // .onDelete('CASCADE')
      table
        .integer('concessionaria_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('concessionarias')
        .onUpdate('CASCADE')
      table
        .string('database')
        .notNullable()
        .unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('empresas')
  }
}

module.exports = EmpresaSchema
