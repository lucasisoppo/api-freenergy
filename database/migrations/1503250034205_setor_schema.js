'use strict'

const Schema = use('Schema')

class SetorSchema extends Schema {
  up () {
    this.create('setors', table => {
      table.increments()
      table.string('descricao').notNullable()
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
        // .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('setors')
  }
}

module.exports = SetorSchema
