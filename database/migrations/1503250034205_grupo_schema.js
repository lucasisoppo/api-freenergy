'use strict'

const Schema = use('Schema')

class GrupoSchema extends Schema {
  up () {
    this.create('grupos', table => {
      table.increments()
      table.string('descricao').notNullable()
      table.decimal('tensao').notNullable()
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
        // .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('grupos')
  }
}

module.exports = GrupoSchema
