'use strict'

const Route = use('Route')

Route.post('sessions', 'SessionController.store')
  .validator('Session')
  .middleware('throttle:10,120')
Route.post('usuarios', 'UsuarioController.store')
  .validator('Usuario')
  .middleware('throttle:10,120')
Route.put('usuarios', 'UsuarioController.update')
  .validator('UsuarioUpdate')
  .middleware('throttle:10,120')
Route.get('usuarios', 'UsuarioController.show')
Route.put('usuarios/alterar-senha', 'UsuarioController.alterPassword')
  .validator('UsuarioAlterPassword')
  .middleware('throttle:10,120')

Route.group(() => {
  Route.get('empresas/:page/:perPage', 'EmpresaController.index')
  Route.get('roles', 'RoleController.index')
  Route.resource('empresas', 'EmpresaController')
    .apiOnly()
    .validator(new Map([[['empresas.store', 'empresas.update'], ['Empresa']]]))
}).middleware('auth')

Route.group(() => {
  Route.post('invites', 'InviteController.store')
    .validator('Invite')
    .middleware('is:admin', 'throttle:15,60')

  Route.resource('dashboard', 'DashboardController')
    .apiOnly()
    .middleware('throttle:15,60')
  Route.get('dashboard/:page/:perPage', 'DashboardController.index')

  Route.resource('equipamentos', 'EquipamentoController')
    .apiOnly()
    .validator(new Map([[['equipamentos.store', 'equipamentos.update'], ['Equipamento']]]))
    .middleware(
      new Map([[['equipamentos.store', 'equipamentos.update'], ['can:equipamento_create']]])
    )
    .middleware('throttle:15,60')
  Route.get('equipamentos/:page/:perPage', 'EquipamentoController.index')

  Route.resource('consumos', 'ConsumoController')
    .apiOnly()
    .validator(new Map([[['consumos.store', 'consumos.update'], ['Consumo']]]))
    .middleware(
      new Map([[['consumos.store', 'consumos.update'], ['can:equipamento_create']]])
    )
    .middleware('throttle:15,60')
  Route.get('consumos/:page/:perPage', 'ConsumoController.index')

  Route.resource('grupos', 'GrupoController')
    .apiOnly()
    .validator(
      new Map([[['grupos.store', 'grupos.update'], ['Grupo']]])
    )
    .middleware(new Map([[['grupos.store', 'grupos.update'], ['is:admin']]]))
    .middleware('throttle:15,60')

  Route.get('grupos/:page/:perPage', 'GrupoController.index')

  Route.resource('setors', 'SetorController')
    .apiOnly()
    .validator(
      new Map([[['setors.store', 'setors.update'], ['Setor']]])
    )
    .middleware(new Map([[['setors.store', 'setors.update'], ['is:admin']]]))
    .middleware('throttle:15,60')

  Route.get('setors/:page/:perPage', 'SetorController.index')

  Route.get('membros', 'MembroController.index')
  Route.put('membros/:id', 'MembroController.update').middleware(
    'is:admin',
    'throttle:15,60'
  )

  Route.get('permissions', 'PermissionController.show')
}).middleware(['auth', 'empresa'])

Route.post('recuperar-senha', 'RecuperarSenhaController.store').middleware(
  'throttle:10,120'
)
Route.put('recuperar-senha', 'RecuperarSenhaController.update').middleware(
  'throttle:10,120'
)

Route.get('concessionarias', 'ConcessionariaController.index')